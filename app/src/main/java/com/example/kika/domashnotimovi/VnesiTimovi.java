package com.example.kika.domashnotimovi;

import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static android.R.attr.data;
import static android.R.attr.entryValues;
import static android.R.attr.value;

public class VnesiTimovi extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public EditText lokacijaPole, timPole;
    public Button save;
    //public String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "raw/";

    private Spinner spinner;
    private static final String[] paths = {"Америка", "Југоисточна Азија", "Кина", "Европа"};
    String lokacija;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vnesi_timovi);

        timPole = (EditText) findViewById(R.id.timPole);
        //lokacijaPole = (EditText) findViewById(R.id.lokacijaPole);
        save = (Button) findViewById(R.id.btnSave);

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(VnesiTimovi.this,
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        ((TextView) parent.getChildAt(0)).setTextColor(Color.GRAY);
        switch (position) {
            case 0:
                lokacija = "Americas";
                break;
            case 1:
                lokacija = "Southeast Asia";
                break;
            case 2:
                lokacija = "China";
                break;
            case 3:
                lokacija = "Europe";
                break;


        }
    }



    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(getApplicationContext(), "Избери регион!", Toast.LENGTH_LONG).show();

    }

        /*File dir = new  File(path);
        dir.mkdirs();*/


    public void Snimi(View view) {

        String snimiZapis = String.valueOf(lokacija+ ", " + String.valueOf(timPole.getText()+ "/n"));
        try {
            FileOutputStream fos = openFileOutput("timovi.txt", MODE_APPEND);
            fos.write(snimiZapis.getBytes());
            fos.close();
            timPole.setText("");
            //lokacijaPole.setText("");
            Toast.makeText(getApplicationContext(), "Внесено", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }

    }


