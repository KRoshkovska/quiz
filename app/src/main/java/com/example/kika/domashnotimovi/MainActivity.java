package com.example.kika.domashnotimovi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void Vnesi(View view) {
        Intent intent = new Intent(MainActivity.this, VnesiTimovi.class);
        startActivity(intent);
    }

    public void Igraj(View view) {
        Intent intent = new Intent(MainActivity.this, Igra.class);
        startActivity(intent);
    }
}
