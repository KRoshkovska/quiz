package com.example.kika.domashnotimovi;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

import static android.R.attr.drawable;
import static android.R.attr.layout;
import static android.R.attr.lines;
import static android.R.attr.packageNames;
import static android.os.SystemClock.sleep;

public class Igra extends AppCompatActivity {

    int bRedovi = 0, red = 0, poeni = 0;
    String izbran = null, lokacija, tim, kliknato;
    String[] niza = new String[500];

    TextView txtPoeni;
    TextView txtVTim;
    ImageView slika, timSlika;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igra);
        try {
            Citanje();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            PostaviZadaca();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void Citanje() throws FileNotFoundException {


        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
            new InputStreamReader(getAssets().open("timovi.txt")));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
               bRedovi++;
                niza[bRedovi] = reader.readLine().toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }

    }

    public void PostaviZadaca() throws FileNotFoundException {

        int x;
        String timImg;

        Random random = new Random();
        red = random.nextInt(bRedovi);
        if (red != 0 ){
        x = red*2-1;
        timImg = "img" + x;
        }
        else {
            timImg = "img" + red;
        }
        izbran = niza[red];
        String [] items = izbran.split(",");
        lokacija = items[0];
        tim = items[1];

        txtPoeni = (TextView) findViewById(R.id.textViewPoeni);
        txtPoeni.setText("Вкупно поени: " + String.valueOf(poeni));
        txtVTim = (TextView) findViewById(R.id.txtTim);
        txtVTim.setText(tim);
        timSlika = (ImageView) findViewById(R.id.imageView2);
        int resId = getResources().getIdentifier(timImg, "drawable" ,getPackageName());
        timSlika.setBackgroundResource(resId);

    }

    public int Proverka() {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout,
                (ViewGroup)findViewById(R.id.toastLayout));

        ImageView slika = (ImageView)layout.findViewById(R.id.imageView);
        TextView toastText = (TextView)layout.findViewById(R.id.textView4);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.RIGHT, 3, 3);
        toast.setDuration(Toast.LENGTH_SHORT);


        if (kliknato.equals(lokacija)) {
            poeni++;
            toastText.setText("Точно");
            slika.setImageResource(R.drawable.yey);
            toast.setView(layout);
            toast.show();

        } else {
            poeni--;
            toastText.setText("Nope");
            slika.setImageResource(R.drawable.noo);
            toast.setView(layout);
            toast.show();
        }
        return poeni;
    }


    public void Izbiranje(View view) {
        kliknato = "Americas";
        Proverka();
        try {
            PostaviZadaca();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void IzborDva(View view) {
        kliknato = "Southeast Asia";
        Proverka();
        try {
            PostaviZadaca();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void IzborTri(View view) {
        kliknato = "China";
        Proverka();
        try {
            PostaviZadaca();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void IzborCetiri(View view) {
        kliknato = "Europe";
        Proverka();
        try {
            PostaviZadaca();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
